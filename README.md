# Database First Workflow Project
### Project demonstrates the use of the database first workflow.

- Create database Pluto
- Run <i>Pluto.sql</i> in Microsoft SQL Server Management Studio to create the database
- Check if ADO.NET added in project. If not create and add it.